import './App.scss';
import 'angular-material';

import 'angular';
import 'angular-ui-router';
import 'restangular';
import 'angular-material/angular-material.css'
import 'angular-animate';
import 'angular-aria';
import 'ngstorage';
import 'angular-sanitize';
import 'ng-youtube-embed';

import routing from './app.config';
import components from './components';

angular.module('app',
    [
        // 'ngSanitize',
        'ngMaterial',
        'ui.router',
        'restangular',
        'ngAnimate',
        'ngYoutubeEmbed',
        'ngAria',
        'ngStorage',
        components,
    ])
    .config(routing);

