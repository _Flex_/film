export default function routing($urlRouterProvider, $locationProvider, RestangularProvider) {
    'ngInject';

    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/top-films');

    RestangularProvider.setBaseUrl('http://www.myapifilms.com/');
    RestangularProvider.setDefaultHeaders({
        'Content-Type': 'application/json'
    });
    RestangularProvider.setFullResponse(true);
}

// routing.$inject = ['$urlRouterProvider', '$locationProvider', 'RestangularProvider'];
