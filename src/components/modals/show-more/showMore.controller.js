export default class ShowMoreController {

    constructor($mdDialog, item) {
        'ngInject';

        this.modal = $mdDialog;
        this.film = item;
    }

    cancel() {
        this.modal.cancel();
    }
}