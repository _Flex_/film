import TopFilmsComponent from './films/top-films/topFilms.component';
import FavoritesFilmsComponent from './films/favorites-films/favoritesFilms.component';
import ChartComponent from './films/chart/chart.component';
import MainComponent from './main/main.component';

export default function config($stateProvider) {
    'ngInject';

    $stateProvider
        .state('main', {
            abstract: true,
            component: MainComponent.name
        })
        .state('top-films', {
            url: '/top-films',
            parent: 'main',
            component: TopFilmsComponent.name
        })
        .state('chart', {
            url: '/chart',
            parent: 'main',
            component: ChartComponent.name
        })
        .state('favorites-films', {
            url: '/favorites-films',
            parent: 'main',
            component: FavoritesFilmsComponent.name
        });
}
