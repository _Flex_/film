import angular from 'angular';

import services from '../services/app.service';
import config from './components.config';
import TopFilmsComponent from './films/top-films/topFilms.component';
import FilmsListComponent from './base/films-list/filmsList.component';
import FavoritesFilmsComponent from './films/favorites-films/favoritesFilms.component';
import ChartComponent from './films/chart/chart.component';
import MainComponent from './main/main.component';

export default angular.module('app.components', [services])
    .config(config)
    .component(MainComponent.name, MainComponent)
    .component(FilmsListComponent.name, FilmsListComponent)
    .component(TopFilmsComponent.name, TopFilmsComponent)
    .component(FavoritesFilmsComponent.name, FavoritesFilmsComponent)
    .component(ChartComponent.name, ChartComponent)
    .name;
