export default class FavoritesFilmsController {

    constructor(filmsService, $mdDialog, $timeout) {
        'ngInject';

        this.$timeout = $timeout;
        this.service = filmsService;
        this.modal = $mdDialog;
        this.films = this.getFilms();
    }

    getFilms() {
        let films = this.service.getFavorites();
        films = Object.values(films);
        films = films.sort((a, b) => (a.ranking > b.ranking) ? 1 : ((b.ranking > a.ranking) ? -1 : 0));
        return films;
    }

    /**
     * @description
     * Callback function for unlike action
     * */
    onRemove() {
        this.$timeout(() => {
            this.films = this.getFilms();
        }, 400);
    }
}