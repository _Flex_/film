import FavoritesFilmsController from "./favoritesFilms.controller";
import FavoritesFilmsTemplate from "./favoritesFilms.html";

export default {
    name: 'favoritesFilmsComponent',
    controller: FavoritesFilmsController,
    template: FavoritesFilmsTemplate
};