import Chart from 'chart.js';
import _ from 'lodash';


export default class ChartController {

    constructor(filmsService) {
        'ngInject';

        this.service = filmsService;
    }

    async $onInit() {
        const films = await this.getData();
        const ctx = document.getElementById("myChart");

        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: films.labels,
                datasets: [{
                    label: 'amount',
                    data: films.data,
                    backgroundColor: 'rgba(54, 162, 235, 0.5)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display:false
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }]
                }
            }
        });
    }

    async getData() {
        const films = await this.service.getFilms();
        const result = {};
        let labels = [];

        _.forEach(films, item => {

            if (!result[item.year]) {
                result[item.year] = [];
                labels.push(item.year);
            }

            result[item.year].push(item);
        });

        labels = labels.sort((a, b) => (+a > +b) ? 1 : ((+b > +a) ? -1 : 0));
        const data = Object.values(result).map(item => {
            return item.length;
        });

        return { labels, data };
    }

}