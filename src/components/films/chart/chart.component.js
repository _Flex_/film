import ChartController from "./chart.controller";
import ChartTemplate from "./chart.html";

export default {
  name: 'chartComponent',
  controller: ChartController,
  template: ChartTemplate
};