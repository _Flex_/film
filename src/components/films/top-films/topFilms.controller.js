export default class TopFilmsController {
    constructor(filmsService, $timeout) {
        'ngInject';

        this.$timeout = $timeout;
        this.service = filmsService;

        this.loading = true;
    }

    async $onInit() {
        this.films = [];

        const res = await this.service.getFilms();
        this.$timeout(() => {
            this.loading = false;
            this.films = res;
        });
    }

    async getFilms() {

    }
}
