import TopFilmsController from './topFilms.controller';
import TopFilmsTemplate from './topFilms.html';

export default {
    name: 'topFilmsComponent',
    controller: TopFilmsController,
    template: TopFilmsTemplate
};