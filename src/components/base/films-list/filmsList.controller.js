import ShowMoreController from "../../modals/show-more/showMore.controller";
import ShowMoreTemplate from "../../modals/show-more/showMore.html";

export default class FilmsListController {

    constructor(filmsService, $mdDialog, $timeout, $mdToast) {
        'ngInject';

        this.service = filmsService;
        this.modal = $mdDialog;
        this.$timeout = $timeout;
        this.$mdToast = $mdToast;
    }

    addToFavorite(item) {
        item.isFavorite = true;
        this.service.addToFavorites(item);

        this.$mdToast.show(
            this.$mdToast.simple()
                .textContent(`Added ${item.title} to favorite`)
                .position('bottom right')
                .hideDelay(3000)
                .parent(angular.element(document.getElementById("toast-container")))
        );
    }

    removeFilm(item) {
        item.isFavorite = false;
        this.service.removeFromFavorite(item);
        this.onRemove();

        this.$mdToast.show(
            this.$mdToast.simple()
                .textContent(`Removed ${item.title} from favorite`)
                .position('bottom right')
                .hideDelay(3000)
                .parent(angular.element(document.getElementById("toast-container")))
        );
    }

    async showMore(ev, film) {
        const res = await this.service.getTrailer(film.idIMDB);

        film.videoId = res.videoId;

        this.modal.show({
            controller: ShowMoreController,
            controllerAs: 'ctrl',
            template: ShowMoreTemplate,
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            locals: {
                item: film
            }
        })
    }
}