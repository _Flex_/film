import FilmsListController from "./filmsList.controller";
import FilmsListTemplate from "./filmsList.html";

export default {
    name: 'filmsList',
    controller: FilmsListController,
    template: FilmsListTemplate,
    bindings: {
        films: '<',
        onRemove: '&'
    }
};