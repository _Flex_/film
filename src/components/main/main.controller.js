export default class MainController {

    constructor($state) {
        'ngInject';

        this.$state = $state;
    }

    goTo(state) {
        if (state.length === 0) {
            return false;
        }

        this.$state.go(state);
    }
}