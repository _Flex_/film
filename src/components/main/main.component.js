import MainTemplate from './main.html';
import MainController from "./main.controller";

export default {
    name: 'mainComponent',
    controller: MainController,
    template: MainTemplate
};