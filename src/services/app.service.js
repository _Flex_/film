import angular from 'angular';
import restangular from 'restangular';
import _ from 'lodash';
import {films, trailers} from "../constants/test-data";

const token = 'c78fc56c-60d2-47ce-b0fc-627b2fd8134b';
const defaultParams = {
    format: 'json',
    token: token
};


class FilmsService {
    constructor(Restangular, $cacheFactory, $localStorage, $timeout) {
        'ngInject';

        this.cache = $cacheFactory(FilmsService.name);
        this.Restangular = Restangular;
        this.$timeout = $timeout;
        this.storage = $localStorage.$default({
            favorites: {},
        });
    }

    /**
     * @description
     * This method clears local storage
     */
    resetLocalStorage() {
        this.storage.$reset({
            favorites: {},
        });
    }

    /**
     * @description
     * Get films from api or from local file
     */
    getFilms() {
        const params = {
            data: 1,
            end: 20,
            ...defaultParams
        };

        /**
         * @description
         * The API does not return all the headers, so this code is commented out.
         * To get data from the API you just need to uncomment it.
         */
        // return new Promise(async (resolve, reject) => {
        //     try {
        //         const res = await this.Restangular.all('imdb').all('top')
        //             .withHttpConfig({cache: this.cache})
        //             .customGET('', params);
        //
        //         let data = _.get(res, 'data.data.movies');
        //
        //         if (!data) {
        //             resolve([]);
        //         }
        //
        //         data = this.checkFilms(films);
        //         resolve(data);
        //
        //     } catch (err) {
        //         reject(err);
        //     }
        // });


        /**
         * @description
         * If API doesn't work just uncommented this code
         */
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const res = this.checkFilms(films);
                resolve(res);
            }, 200);
        });
    }

    /**
     * @description
     * Get trailer for the film by title
     */
    getTrailer(id) {
        const params = {
            idIMDB: id,
            count: 8,
            ...defaultParams
        };
        // return this.Restangular.one('trailerAddict').all('taapi').customGET('', params);
        return new Promise((resolve, reject) => {
            this.$timeout(() => {
                resolve(trailers[id]);
            }, 500);
        });
    }

    /**
     * @description
     * Add to film isFavorite field.
     */
    checkFilms(items) {
        _.forEach(items, item => {
            item.isFavorite = this.isFavorite(item);
        });

        return items;
    }

    /**
     * @description
     * Get additional info about film by idIMDB.
     * @returns {Promise}
     */
    getInfo(id) {
        const params = {
            idIMDB: id,
            ...defaultParams
        };
        return this.Restangular.one('imdb').all('idIMDB').customGET('', params);
    }

    /**
     * @description
     * Add film to favorite films list.
     */
    addToFavorites(item) {
        if (this.isFavorite(item)) {
            return false;
        }

        this.storage.favorites[item.idIMDB] = item;
    }

    /**
     * @description
     * Remove the film from favorite films list.
     */
    removeFromFavorite(item) {
        delete this.storage.favorites[item.idIMDB];
    }

    /**
     * @description
     * checks if the film is in the favorites list
     * @returns {boolean}
     * */
    isFavorite(item) {
        return !!this.storage.favorites[item.idIMDB];
    }

    /**
     * @description
     * Return favorite films list.
     * @returns {favorites|{}}
     */
    getFavorites() {
        return this.storage.favorites;
    }
}

export default angular.module('app.services', [restangular])
    .service('filmsService', FilmsService)
    .name;
